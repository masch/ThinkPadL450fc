
ThinkPadL450fc
==============

Linux kernel module to control the CPU fan of a Lenovo ThinkPad L450 (20DS0001GE)
laptop.

As Lenovo's build in fan controlling is quit conservative at the lower temperature
range, this module controls the vantilation less restrictive as the following table
shows:

| fan speed              |  off  | 2500rpm | 2800rpm | 3100rpm | 3300rpm | 3500rpm |
| :---                   | :---: |  :---:  |  :---:  |  :---:  |  :---:  |  :---:  |
| step up temp. Lenovo   |  50°C |   60°C  |   70°C  |   80°C  |   90°C  |    -    |
| step down temp. Lenovo |   -   |   40°C  |   50°C  |   60°C  |   70°C  |   80°C  |
| step up temp. module   |  58°C |   64°C  |   68°C  |   72°C  |   75°C  |    -    |
| step down temp. module |   -   |   46°C  |   58°C  |   64°C  |   68°C  |   72°C  |

Furthermore the CPU temperature is a bit low-pass filtered to restrain hypersensitive
temperature sensors.


Restrictions
------------
The BIOS is checked regarding system vendor, product name and product version,
so that the module will refuse to work on other hardware platforms than Lenovo
ThinkPad L450 (20DS0001GE).

The module just allows to set the verbosity level of kernel message output (0..3,
default=0) and the polling interval of processing (1000..10000ms, default=3000ms).
The temperature thresholds and fan speed steps are hard-coded and have to be changed
within the source code if required.


Example usage on Debian 8
-------------------------
- **Generate source code documentation:** `doxygen ThinkPadL450fc.doxyfile`
- **Generate deb package:** `sudo dkms.sh`
- **Install deb package:** `sudo dkms -i *.deb`
- **View module information:** `sudo modinfo tpl450fc`
- **Load module manually:** `sudo modprobe tpl450fc`, or `sudo insmod tpl450fc.ko`
- **Unload module manually:** `sudo rmmod tpl450fc`
- **Load module automatically on system start:**
  1. *Add module name to /etc/modules:* `sudo sh -c "echo '\n'tpl450fc >> /etc/modules"`
  2. *Add module options to /etc/modprobe.d/tpl450fc.conf:* `sudo sh -c "echo options tpl450fc verbose=1 interval=5000 > /etc/modprobe.d/tpl450fc.conf"`
- **Review module output**: `dmesg | grep tpl450fc`

Credits
-------
ThinkPadL450fc based on [ProBook6475fc](https://gitlab.com/masch/ProBook6475fc).
