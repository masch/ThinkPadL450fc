#!/bin/sh
#
# Script to generate a dkms deb package for tpl450fc
#
MODULE="tpl450fc"
VERSION="2016.08.23"
#
mkdir /usr/src/$MODULE-$VERSION/
cp dkms.conf ../src/Makefile ../src/$MODULE.c /usr/src/$MODULE-$VERSION/
#
dkms add -m $MODULE -v $VERSION
dkms mkdeb -m $MODULE -v $VERSION --source-only
#
cp /var/lib/dkms/$MODULE/$VERSION/deb/*.deb .
#
dkms remove -m $MODULE -v $VERSION --all
rm -r /usr/src/$MODULE-$VERSION
